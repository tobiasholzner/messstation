FROM adoptopenjdk:11-jre-hotspot

RUN mkdir /opt/app
COPY ./build/libs/*.jar /opt/app

ENV LOG_LEVEL=INFO
ENV LOG_FILE=./server.log
ENV JSON_FILE=./measurements.log
ENV PORT=1024

EXPOSE $PORT

CMD ["java", "-jar", "/opt/app/studienarbeit-1.0-SNAPSHOT.jar"]
