package de.thro.vv.studienarbeit.configuration;

import org.junit.jupiter.api.Test;

class TestCustomLogger {
    @Test
    void testCustomLogger() {
        /**
         * check log messages manualy
         */
        CustomLogger.getLogger().severe("SEVERE TEST MESSAGE");
        CustomLogger.getLogger().info("INFO TEST MESSAGE");
    }
}

