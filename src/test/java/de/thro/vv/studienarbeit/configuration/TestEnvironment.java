package de.thro.vv.studienarbeit.configuration;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestEnvironment {
    @Test
    void testEnvironment() {
        Environment.readEnvironment();
        assertEquals(Environment.getLogLevel(), System.getenv("LOG_LEVEL"));
        assertEquals(Environment.getLogFile(), System.getenv("LOG_FILE"));
        assertEquals(Environment.getJsonFile(), System.getenv("JSON_FILE"));
        assertEquals(Environment.getPORT(), Integer.parseInt(System.getenv().getOrDefault("PORT", "1024")));
        assertEquals(100, Environment.getMaxSensors());
    }
}
