package de.thro.vv.studienarbeit.model;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class TestMeasurement {
    @Test
    void testMeasurement() {
        Measurement m = new Measurement(5, Measurement.Unit.hectopascal,
                Measurement.Type.pressure, LocalDateTime.now());

        assertEquals(5, m.getValue());
        assertEquals(Measurement.Unit.hectopascal, m.getUnit());
        assertEquals(Measurement.Type.pressure, m.getType());
        String mJson = m.toJson();
    }
}
