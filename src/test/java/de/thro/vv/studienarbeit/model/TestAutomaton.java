package de.thro.vv.studienarbeit.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestAutomaton {
    private Automaton automaton;

    @BeforeEach
    void setup() {
        automaton = new Automaton();
    }

    @Test
    @DisplayName("getCurrentState Test")
    void getCurrentStateTest() {
        assertEquals(Automaton.State.WaitingForClient, automaton.getCurrentState());
    }

    @Test
    @DisplayName("Waiting For Client -> Terminated (Test)")
    void waitingForClientToTerminated() {
        automaton.nextState(Message.Type.SensorHello);
        automaton.nextState(Message.Type.Terminate);
        assertEquals(Automaton.State.Terminated, automaton.getCurrentState());

        automaton = new Automaton();
        automaton.nextState(Message.Type.SensorHello);
        automaton.nextState(Message.Type.Acknowledge);
        automaton.nextState(Message.Type.Measurement);
        automaton.nextState(Message.Type.Terminate);
        assertEquals(Automaton.State.Terminated, automaton.getCurrentState());

        automaton = new Automaton();
        automaton.nextState(Message.Type.Terminate);
        automaton.nextState(Message.Type.Terminate);
        assertEquals(Automaton.State.Terminated, automaton.getCurrentState());
    }

    @Test
    @DisplayName("Waiting For Client -> Error (Test)")
    void waitingForClientToError() {
        automaton.nextState(Message.Type.Terminate);
        automaton.nextState(Message.Type.SensorHello);
        automaton.nextState(Message.Type.Measurement);
        automaton.nextState(Message.Type.Acknowledge);
        assertEquals(Automaton.State.Error, automaton.getCurrentState());

        automaton = new Automaton();
        automaton.nextState(Message.Type.Measurement);
        assertEquals(Automaton.State.Error, automaton.getCurrentState());

        automaton = new Automaton();
        automaton.nextState(Message.Type.Acknowledge);
        assertEquals(Automaton.State.Error, automaton.getCurrentState());
    }
}