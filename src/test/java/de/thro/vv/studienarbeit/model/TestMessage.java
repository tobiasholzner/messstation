package de.thro.vv.studienarbeit.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TestMessage {
    @Test
    void testMessage() {
        Message m = new Message(Message.Type.StationHello, "hello");
        assertEquals("hello", m.getPayload());
        assertEquals(Message.Type.StationHello, m.getType());
    }
}
