package de.thro.vv.studienarbeit.configuration;

import java.io.IOException;
import java.util.logging.*;

/**
 * creates a custom logger which writes log messages to the file mentioned in Environment class
 * only one logger for whole application
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class CustomLogger {
    private static final Logger log = Logger.getLogger(CustomLogger.class.getName());

    private CustomLogger() {}

    public static Logger getLogger() {
        return log;
    }

    public static void createLog() {
        /**
         * set log file format
         * YEAR-MONTH-DAY HOUR:MINUTES:SECONDES LOGLEVEL MESSAGE
         */
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS %4$s %5$s%6$s%n");

        try {
            Handler handler = new FileHandler(Environment.getLogFile(), true);
            log.addHandler(handler);
            handler.setFormatter(new SimpleFormatter());
            log.setLevel(Level.parse(Environment.getLogLevel()));
        } catch (IOException | SecurityException e) {
            System.err.println("ERROR: logging to " + Environment.getLogFile() +
                    " is not possible!");
        }
    }
}
