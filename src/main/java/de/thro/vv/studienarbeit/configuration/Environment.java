package de.thro.vv.studienarbeit.configuration;

/**
 * read environment variables
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class Environment {
    private static final int MAX_SENSORS = 100;
    private static int port;
    private static String jsonFile;
    private static String logFile;
    private static String logLevel;

    private Environment() {}

    public static int getMaxSensors() {
        return MAX_SENSORS;
    }

    public static int getPORT() {
        return port;
    }

    public static String getJsonFile() {
        return jsonFile;
    }

    public static String getLogFile() {
        return logFile;
    }

    public static String getLogLevel() {
        return logLevel;
    }

    public static void readEnvironment() {
        try {
            port = Integer.parseInt(System.getenv().getOrDefault("PORT", "1024"));
            jsonFile = System.getenv("JSON_FILE");
            logFile = System.getenv("LOG_FILE");
            logLevel = System.getenv("LOG_LEVEL");
        } catch (Exception e) {
            System.err.println("ERROR: can not read environment variables");
            System.exit(1);
        }
    }
}