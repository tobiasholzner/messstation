package de.thro.vv.studienarbeit.model;

/**
 * protocol automaton
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class Automaton {
    private State currentState;

    public enum State {
        WaitingForClient,
        WaitingForAcknowledge,
        WaitingForMeasurement,
        Terminated,
        Error
    }

    /**
     * state transition table to implement automaton
     */
    private State[][] transition = {
        // WaitingForClient WaitingForAcknowledge WaitingForMeasurement Terminate Error
        {State.WaitingForAcknowledge, State.Error, State.Error, State.Terminated, State.Error}, // SensorHello
        {State.Error, State.Error, State.Error, State.Terminated, State.Error}, // StationHello
        {State.Error, State.Error, State.Error, State.Terminated, State.Error}, // StationReady
        {State.Error, State.WaitingForMeasurement, State.Error, State.Terminated, State.Error}, // Acknowledge
        {State.Error, State.Error, State.WaitingForMeasurement, State.Terminated, State.Error}, // Measurement
        {State.Error, State.Terminated, State.Terminated, State.Terminated, State.Terminated}, // Terminate
        {State.Terminated, State.Terminated, State.Terminated, State.Terminated, State.Terminated}, // TerminateStation
        {State.Error, State.Error, State.Error, State.Terminated, State.Error}, // Error
    };

    public Automaton() {
        this.currentState = State.WaitingForClient;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void nextState(Message.Type input) {
        this.currentState = nextState(currentState, input);
    }

    private State nextState(State current, Message.Type input) {
        return transition[input.ordinal()][current.ordinal()];
    }
}
