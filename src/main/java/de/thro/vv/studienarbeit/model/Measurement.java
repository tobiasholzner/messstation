package de.thro.vv.studienarbeit.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Measurement class with value, type, unit and the measurement time
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class Measurement {
    private long value;
    private Unit unit;
    private Type type;
    private String timestamp;

    public enum Type {
        temperature,
        humidity,
        pressure,
        count,
        flow_rate,
        energy
    }

    public enum Unit {
        celsius,
        kelvin,
        percent,
        hectopascal,
        units,
        cms,
        kwh
    }

    public long getValue() {
        return value;
    }

    public Unit getUnit() {
        return unit;
    }

    public Type getType() {
        return type;
    }

    public Measurement(long value, Unit unit, Type type, LocalDateTime time) {
        timestamp = time.toString().substring(0, 19);
        this.value = value;
        this.type = type;
        this.unit = unit;
    }

    public static Measurement fromJson(String jsonMeasurement) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(jsonMeasurement, Measurement.class);
    }

    public String toJson() {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Measurement))
            return false;

        Measurement m = (Measurement) obj;
        return (m.value == this.value && m.type == this.type && m.unit == this.unit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, unit, type, timestamp);
    }
}