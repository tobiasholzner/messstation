package de.thro.vv.studienarbeit.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.util.Objects;

/**
 * Message pattern for communication between sensor and station
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class Message {
    private Type type;
    private String payload;

    public enum Type {
        SensorHello,
        StationHello,
        StationReady,
        Acknowledge,
        Measurement,
        Terminate,
        TerminateStation,
        TimeOut
    }

    public Message(Type type, String payload) {
        this.type = type;
        this.payload = payload;
    }

    public Message(String init) {
        fromString(init);
    }

    public Type getType() {
        return type;
    }

    public String getPayload() {
        return payload;
    }

    private void fromString(String from) {
        Gson gson = new GsonBuilder().create();
        JsonObject object = gson.fromJson(from, JsonObject.class);
        this.payload = gson.toJson(object.get("payload").getAsJsonObject());
        this.type = Type.valueOf(object.get("type").getAsString());
    }

    @Override
    public String toString() {
        Gson gson = new GsonBuilder().create();
        JsonObject object = new JsonObject();
        object.addProperty("type", type.name());
        object.add("payload", gson.fromJson(payload, JsonObject.class));
        return gson.toJson(object);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return type == message.type && payload.equals(message.payload);
     }

    @Override
    public int hashCode() {
        return Objects.hash(type, payload);
    }
}