package de.thro.vv.studienarbeit;

import de.thro.vv.studienarbeit.configuration.CustomLogger;
import de.thro.vv.studienarbeit.configuration.Environment;
import de.thro.vv.studienarbeit.server.Server;

import java.util.concurrent.ExecutorService;

/**
 * application start with this Main class
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class Main {
    public static void main(String[] args) {
        Environment.readEnvironment();
        CustomLogger.createLog();

        CustomLogger.getLogger().info("operating system: " + System.getProperty("os.name") + " " +
                System.getProperty("os.version"));
        CustomLogger.getLogger().info("jdk version: " + System.getProperty("java.version"));
        CustomLogger.getLogger().info("jre version: " + System.getProperty("java.vendor"));
        CustomLogger.getLogger().info("username: " + System.getProperty("user.name"));
        CustomLogger.getLogger().info("environment variable: LOG_FILE=" + Environment.getLogFile());
        CustomLogger.getLogger().info("environment variable: LOG_LEVEL=" + Environment.getLogLevel());
        CustomLogger.getLogger().info("environment variable: JSON_FILE=" + Environment.getJsonFile());

        Server server = new Server();
        ExecutorService executorService = server.getExecutorService();
        executorService.execute(server);
    }
}