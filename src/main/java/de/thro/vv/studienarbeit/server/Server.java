package de.thro.vv.studienarbeit.server;

import de.thro.vv.studienarbeit.configuration.CustomLogger;
import de.thro.vv.studienarbeit.configuration.Environment;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Server implementation
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class Server implements Runnable {
    private ExecutorService executorService = Executors.newFixedThreadPool(Environment.getMaxSensors());
    private MeasurementWriter measurementWriter;
    private List<SensorHandler> sensorHandlers;

    public Server() {
        measurementWriter = new MeasurementWriter();
        sensorHandlers = new LinkedList<>();
    }

    @Override
    public void run() {
        try (ServerSocket server = new ServerSocket(Environment.getPORT())) {
            server.setSoTimeout(20000);
            CustomLogger.getLogger().info("Server started on port: " + Environment.getPORT());
            executorService.execute(measurementWriter);

            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Socket socket = server.accept();
                    ForwarderReceiver forwarderReceiver = new ForwarderReceiver(socket);
                    SensorHandler sensorHandler = new SensorHandler(forwarderReceiver, measurementWriter.getMeasurementQueue());
                    sensorHandlers.add(sensorHandler);
                    executorService.execute(sensorHandler);
                } catch (SocketTimeoutException e) {
                    if (getNumberOfOpenSockets() <= 0) {
                        CustomLogger.getLogger().info("server timeout");
                        Thread.currentThread().interrupt();
                    }
                }
            }
        } catch (Exception e) {
            CustomLogger.getLogger().severe(e.getMessage());
        }
        executorService.shutdownNow();
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    int getNumberOfOpenSockets() {
        List<SensorHandler> handlersToRemove = new LinkedList<>();
        for (SensorHandler handler : sensorHandlers) {
            if (handler.getHandlerSocket().isClosed()) {
                handlersToRemove.add(handler);
            }
        }
        for (SensorHandler handler : handlersToRemove) {
            sensorHandlers.remove(handler);
        }
        return sensorHandlers.size();
    }
}
