package de.thro.vv.studienarbeit.server;

import de.thro.vv.studienarbeit.configuration.CustomLogger;
import de.thro.vv.studienarbeit.model.Automaton;
import de.thro.vv.studienarbeit.model.Message;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

/**
 * SensorHandler
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class SensorHandler implements Runnable {

    private final ForwarderReceiver forwarderReceiver;
    private final BlockingQueue<String> blockingQueue;
    private final Automaton automaton;
    private final Socket socket;

    public SensorHandler(ForwarderReceiver forwarderReceiver, BlockingQueue<String> blockingQueue){
        this.forwarderReceiver = forwarderReceiver;
        this.blockingQueue = blockingQueue;
        this.automaton = new Automaton();
        this.socket = forwarderReceiver.getSocket();
    }

    public Socket getHandlerSocket() {
        return socket;
    }

    @Override
    public void run() {
        try {
            handleSensor(forwarderReceiver);
            forwarderReceiver.close();
            CustomLogger.getLogger().info("sensor disconnected");
            forwarderReceiver.close();
        } catch (IOException e) {
            CustomLogger.getLogger().severe(e.getMessage());
        } catch (InterruptedException e) {
            CustomLogger.getLogger().severe("interrupted");
            Thread.currentThread().interrupt();
        }
    }

    private void handleSensor(ForwarderReceiver forwarderReceiver) throws InterruptedException {
        CustomLogger.getLogger().info("station waiting for SensorHello...");
        Message message = forwarderReceiver.receive();
        if(!message.getType().equals(Message.Type.SensorHello)){
            CustomLogger.getLogger().severe("SensorHello not received: closing connection");
            return;
        }
        CustomLogger.getLogger().info("SensorHello  received: sending StationHello...");
        forwarderReceiver.forward(new Message(Message.Type.StationHello, "{}"));
        do {
            message = forwarderReceiver.receive();
            Message.Type messageType = message.getType();
            automaton.nextState(messageType);
            switch(messageType){
                case Measurement:
                    CustomLogger.getLogger().info("received new Measurement: into blockingQueue...");
                    blockingQueue.put(message.getPayload());
                    forwarderReceiver.forward(new Message(Message.Type.Acknowledge, "{}"));
                    break;
                case SensorHello:
                    CustomLogger.getLogger().info("received new SensorHello: sending StationHello...");
                    forwarderReceiver.forward(new Message(Message.Type.StationHello, "{}"));
                    break;
                case Acknowledge:
                    CustomLogger.getLogger().info("received Acknowledge: Server ready");
                    forwarderReceiver.forward(new Message(Message.Type.StationReady, "{}"));
                    break;
                case Terminate:
                case TerminateStation:
                    CustomLogger.getLogger().info("received Terminate: closing connection to Sensor");
                    forwarderReceiver.forward(new Message(Message.Type.Acknowledge, "{}"));
                    break;
                default:
                    CustomLogger.getLogger().info("unknown message type");
            }
        } while (automaton.getCurrentState() != Automaton.State.Error ||
                automaton.getCurrentState() != Automaton.State.Terminated);
    }
}
