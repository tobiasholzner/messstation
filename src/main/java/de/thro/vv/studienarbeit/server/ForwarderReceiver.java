package de.thro.vv.studienarbeit.server;

import de.thro.vv.studienarbeit.configuration.CustomLogger;
import de.thro.vv.studienarbeit.model.Message;

import java.io.Closeable;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class ForwarderReceiver implements Closeable {
    private Socket socket;
    private PrintStream writeToServer;
    private Scanner readFromServer;

    public ForwarderReceiver(Socket peer) {
        try {
            this.socket = peer;
            this.writeToServer = new PrintStream(peer.getOutputStream());
            this.readFromServer = new Scanner(peer.getInputStream());
        } catch (IOException e) {
            CustomLogger.getLogger().severe(e.getMessage() + e);
            System.exit(1);
        }
    }

    public Socket getSocket() {
        return socket;
    }

    @Override
    public void close() throws IOException {
        this.readFromServer.close();
        this.writeToServer.close();
        this.socket.close();
    }

    public void forward(Message message) {
        CustomLogger.getLogger().info(() -> "sent: " + message.toString());
        writeToServer.println(message.toString());
        writeToServer.flush();
    }

    public Message receive() {
        try {
            while (!readFromServer.hasNextLine()) {
                TimeUnit.SECONDS.sleep(1);
            }
        } catch (InterruptedException e) {
            CustomLogger.getLogger().severe(e.getMessage() + e);
            Thread.currentThread().interrupt();
            System.exit(1);
        }

        Message response = new Message(readFromServer.nextLine());
        String responseString = response.toString();
        CustomLogger.getLogger().info(() -> "received: " + responseString);
        return response;
    }
}
