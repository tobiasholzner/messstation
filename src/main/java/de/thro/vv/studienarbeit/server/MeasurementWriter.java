package de.thro.vv.studienarbeit.server;

import de.thro.vv.studienarbeit.configuration.CustomLogger;
import de.thro.vv.studienarbeit.configuration.Environment;

import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Tobias Holzner
 * @version 1.0.0
 */
public class MeasurementWriter implements Runnable {
    private BlockingQueue<String> measurementQueue;

    public MeasurementWriter() {
        measurementQueue = new LinkedBlockingQueue<>();
    }

    @Override
    public void run() {
        try (Writer fileWriter = Files.newBufferedWriter(Paths.get(Environment.getJsonFile()),
                Charset.forName("windows-1252"));) {

            while(!Thread.currentThread().isInterrupted()){
                try{
                    CustomLogger.getLogger().info("reading from measurementQueue");
                    String line = measurementQueue.take();
                    CustomLogger.getLogger().info("write to file");
                    fileWriter.write("\n" + line);
                    fileWriter.flush();
                } catch (InterruptedException e){
                    CustomLogger.getLogger().severe("interrupted MeasurementWriter");
                    Thread.currentThread().interrupt();
                }
            }
        } catch (Exception e) {
            CustomLogger.getLogger().severe(e.getMessage());
        }
    }

    public BlockingQueue<String> getMeasurementQueue() {
        return measurementQueue;
    }
}
