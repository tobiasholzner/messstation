# Tobias Holzner Messstation

Bei der Messtation handelt es sich um einen Server, welcher von Temperatursensoren per
TCP Verbindung Messdaten übermitteln bekommt. Es wurde ein eigener Automat erstellt um die
Verbindung zwischen Senosoren und Server zu handhaben. Der Server erzeugt für jeden sich verbindenden
Sensor einen neuen Thread und schreibt die Daten, welche im JSON Format übertragen werden in eine Datei,
welche durch Umgebungsvariablen angegeben werden kann.

## Installationsanleitung

### Docker Image ausführen
Es wurde ein Docker Image erstellt. Dieses können Sie wie folgt ausführen:

- `docker run --name measurement-station -p 1024:1024 registry.gitlab.com/tobiasholzner/messstation:latest`
